#!/usr/bin/env bash
WORKDIR="$( cd "$(dirname "$0")" ; pwd -P )"
PROJECT=suafs2gdrive
cd $WORKDIR

pip install virtualenv
VENV=$(which virtualenv)

if [ ! -f "$VENV" ]; then
	echo "virtualenv command not found, please install: pip install virtualenv"
	exit 1
fi

# For the main suparenting.py
$VENV -p python3 env
source env/bin/activate
pip3.7 install --upgrade pip
pip3.7 install -r requirements.txt
deactivate
echo Installation done, \"./${PROJECT} -h\" for usage
