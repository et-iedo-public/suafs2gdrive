# WARNING
This is very work in progress, use at your own risk!

# Getting started

Tested on Mac only so far

Use -h for usage, but most things are autodetected.  At first run it'll open the
browser to authenticate against google.  Copy the token shown in the browser and
then run the command "gdrive ..." mentioned in the output. 

At the next run, it should not prompt for the Google Authentication anymore.

```
LOGLEVEL=DEBUG ./suafs2gdrive.py
```

# TODO

Work on the groups stuff next. Some braindump:


```(venv) golfieri@MacBook-Pro-7:/afs/ir/users/g/o/golfieri$ fs la
Access list for . is
Normal rights:
  golfieri rlidwka
  system:administrators rlidwka
  system:campushosts l
  system:www-servers l
(venv) golfieri@MacBook-Pro-7:/afs/ir/users/g/o/golfieri$ pts members
pts: Missing required parameter '-nameorid'
(venv) golfieri@MacBook-Pro-7:/afs/ir/users/g/o/golfieri$ cd /afs/ir/group/iedo
(venv) golfieri@MacBook-Pro-7:/afs/ir/group/iedo$ ls
WWW        cgi-bin    db_private
(venv) golfieri@MacBook-Pro-7:/afs/ir/group/iedo$ pts members
pts: Missing required parameter '-nameorid'
(venv) golfieri@MacBook-Pro-7:/afs/ir/group/iedo$ fs la
Access list for . is
Normal rights:
  group-iedo.cgi l
  iedo-admins rlidwka
  service.mysql rl
  system:administrators rlidwka
  system:dept-admin rlidwka
  system:www-servers l
(venv) golfieri@MacBook-Pro-7:/afs/ir/group/iedo$ pts members -nameorid iedo-admins
Members of iedo-admins (id: -44508) are:
  spinto
  golfieri
  bnbarnes
(venv) golfieri@MacBook-Pro-7:/afs/ir/group/iedo$ 
(venv) golfieri@MacBook-Pro-7:/afs/ir/group/iedo$ 
(venv) golfieri@MacBook-Pro-7:/afs/ir/group/iedo$ pts members workgroup:uit-et-eit_iedo-staff
Members of workgroup:uit-et-eit_iedo-staff (id: -44582) are:
  spinto
  aguzhavi
  lonlone
  idouglas
  golfieri
  bnbarnes```