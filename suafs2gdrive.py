#!env/bin/python3
# -*- coding: utf-8 -*-

__author__ = "UIT ET IEDO - Stanford University <uit-et-eit-iedo-staff@office365stanford.onmicrosoft.com>"
__license__ = "Apache License 2.0"
"""
    suafs2gdrive
    ~~~~~~~~
    Migrate your Stanford AFS home dir to gDrive
"""
import sys, os, argparse, platform, subprocess, re
import logging

logging.basicConfig(
    stream=sys.stdout,
    level=os.environ.get("LOGLEVEL", "INFO"),
    format="%(levelname)s %(message)s",
)
logger = logging.getLogger(__name__)


class Migrator(object):
    src = None
    dest = None

    def __init__(self, sunet=None):
        self.src = SuAFS(sunet)
        self.dest = SuGDrive(sunet)
        self.setup()

    def setup(self):
        self.dest.setup_dest_dir()

    def migrate(self):
        logger.debug("OS_SPECIFICS => {}".format(self.src.OS_SPECIFICS))
        # Usually users have a .backup folder that doesn't play well with gdrive client, and .gdriveignore does not work.
        BACKUP_ENABLED = (
            True
            if os.path.exists(os.path.join(self.src.AFS_USER_HOME, ".backup"))
            else False
        )
        if BACKUP_ENABLED:
            # Removing such .backup mountpoint
            if os.system(self.src.os_spec("REMOVE_AFS_BACKUP_FOLDER")) != 0:
                logger.debug(
                    "Failed command {}".format(
                        self.src.os_spec("REMOVE_AFS_BACKUP_FOLDER")
                    )
                )
        # Main migration sync
        command = (
            "gdrive sync upload --keep-local --delete-extraneous "
            + self.src.AFS_USER_HOME
            + " "
            + self.dest.DEST_DIR_ID
        )
        exit_code = os.system(command)
        if exit_code != 0:
            logger.debug(
                'Failed command {} to migrate {} to destination dir on gDrive "{}" ({}):'.format(
                    command,
                    self.src.AFS_USER_HOME,
                    self.dest.DEST_DIR,
                    self.dest.DEST_DIR_ID,
                )
            )
        if BACKUP_ENABLED:
            # Restore such .backup mount once done.
            if (
                os.system(
                    self.src.os_spec("RESTORE_AFS_BACKUP_FOLDER").replace(
                        "SUNET", self.src.SUNET
                    )
                )
                != 0
            ):
                logger.debug(
                    "Failed command {}".format(
                        self.src.os_spec("RESTORE_AFS_BACKUP_FOLDER")
                    )
                )


class SuDatastore(object):
    SUNET = None
    HOME = None
    OS = None
    OS_SPECIFICS = {
        "Darwin": {"HOME_PATH": "/afs/ir/users/", "DEFAULT_BROWSER_CMD": "open"}
    }

    def __init__(self, sunet=None):
        self.OS = platform.system()
        self.SUNET = sunet or os.environ.get("USER")
        self.HOME = os.environ.get("HOME")

    def os_spec(self, key):
        logger.debug(
            "os_spec() is running {} => {}".format(
                key, self.OS_SPECIFICS.get(self.OS).get(key)
            )
        )
        return self.OS_SPECIFICS.get(self.OS).get(key)

    def os_spec_cmd(self, key):
        return self.os_spec(key).split()

    def shell_output(self, command, **kwargs):
        options = {"stderr": subprocess.STDOUT, "shell": True}
        options.update(kwargs)
        return subprocess.check_output(command, **options)


class SuAFS(SuDatastore):
    AFS_USER_HOME = None
    AFS_HOME_OWNER = None

    def __init__(self, sunet=None, afs_user_home=None):
        super().__init__(sunet)
        self.OS_SPECIFICS["Darwin"].update(
            {
                "AKLOG": "aklog",
                "FS_LA": "fs la",
                "FS_GETCALLERACCESS": "fs getcalleraccess",
                "KLIST_TEST_VALID_TICKET": "klist -t",
                "REMOVE_AFS_BACKUP_FOLDER": "fs rm .backup",
                "RESTORE_AFS_BACKUP_FOLDER": "fs mk .backup user.SUNET.backup",
            }
        )
        if afs_user_home:
            self.AFS_USER_HOME = afs_user_home
        else:
            self.AFS_USER_HOME = os.path.join(
                self.os_spec("HOME_PATH"), self.SUNET[0], self.SUNET[1], self.SUNET
            )
        if subprocess.call(self.os_spec_cmd("KLIST_TEST_VALID_TICKET")) != 0:
            raise Exception(
                'User {0} has no valid kerberos principal, obtain it with "kinit {0}"'.format(
                    self.SUNET
                )
            )
        if subprocess.call(self.os_spec_cmd("AKLOG")) != 0:
            raise Exception('aklog was not successful for user {0}"'.format(self.SUNET))
        if not os.path.isdir(self.AFS_USER_HOME):
            raise Exception(
                "{} is not mounted, or path is wrong for user {}'s home".format(
                    self.AFS_USER_HOME, self.SUNET
                )
            )
        os.chdir(self.AFS_USER_HOME)
        acl_output = self.shell_output(self.os_spec("FS_LA"))
        logger.debug("fs la => {}".format(acl_output))
        non_system_matches = re.findall(
            rb"^\s+(?!.*system:).*$", acl_output, re.MULTILINE
        )
        owner_match = re.search(
            rb"^\s+(?!.*system:).*rlidwka.*$", acl_output, re.MULTILINE
        ).group()
        owner_detected = owner_match.split()[0]
        users_detected = [x.split()[0] for x in non_system_matches]
        self.AFS_HOME_OWNER = owner_detected
        if (
            not re.search(
                rb"^Callers access to \. is rlidwka$",
                self.shell_output(self.os_spec("FS_GETCALLERACCESS")),
            )
            and self.AFS_HOME_OWNER != self.SUNET
        ):
            raise Exception(
                'Principal {0} does not seem to be the rightful owner of {1}. User {2} was detected as owner instead"'.format(
                    self.SUNET, self.AFS_USER_HOME, self.AFS_HOME_OWNER
                )
            )


class SuGDrive(SuDatastore):
    DEST_DIR = None

    def __init__(self, sunet=None, dest_home_path="imported_AFS"):
        super().__init__(sunet)
        self.OS_SPECIFICS["Darwin"].update(
            {
                "MISSING_TOKEN": """You don't seem to have yet granted access to your Stanford gDrive. Please allow access
    in the browser window, and then keep following the instructions here.""",
                "HOW_TO_APPLY_TOKEN": """After you have copied the token from the browser window that is going to be displayed after \n \
    you allowed access, run:

        gdrive list

    from your command line and paste it when asked.
    Press Enter to continue.  Re-run me later as:

        ./suafs2gdrive.py -u

    and I will sync your home folder.""",
                "GDRIVE_INSTALL_STEPS": 'Run "brew install gdrive" in your shell',
                "WHICH_GDRIVE": "which -s gdrive",
                "GDRIVE_LIST": "gdrive list",
                "GDRIVE_MKDIR": "gdrive mkdir 'DIRECTORY'",
                "CHECK_IF_DIRECTORY_EXIST_IN_ROOT": "gdrive list -q \"name = 'DIRECTORY' and 'root' in parents and trashed=false\"",
            }
        )

        if subprocess.call(self.os_spec_cmd("WHICH_GDRIVE")) != 0:
            raise Exception(
                'Gdrive CLI app not installed, please install it. {0}"'.format(
                    self.os_spec("GDRIVE_INSTALL_STEPS")
                )
            )
        if not os.path.exists(os.path.join(self.HOME, ".gdrive/token_v2.json")):
            print(self.os_spec("MISSING_TOKEN"))
            try:
                self.shell_output(self.os_spec("GDRIVE_LIST"), input=b"\n")
            except subprocess.CalledProcessError as e:
                print(self.os_spec("HOW_TO_APPLY_TOKEN"))
                input()
                auth_url = re.search(
                    rb"Go to the following url in your browser:\n(.*)$",
                    e.output,
                    re.MULTILINE,
                ).groups()[0]
                auth_url = (
                    auth_url.decode("utf-8")
                    + "&login_hint="
                    + self.SUNET
                    + "@stanford.edu"
                )
                logger.debug("auth url => {}".format(auth_url))
                os.system(
                    self.os_spec("DEFAULT_BROWSER_CMD") + " '{}'".format(auth_url)
                )

    def setup_dest_dir(self, dirname="AFS_HOME"):
        self.DEST_DIR = dirname
        listing = self.shell_output(
            self.os_spec("CHECK_IF_DIRECTORY_EXIST_IN_ROOT").replace(
                "DIRECTORY", dirname
            )
        )
        if dirname.encode() not in listing:
            output = self.shell_output(
                self.os_spec("GDRIVE_MKDIR").replace("DIRECTORY", dirname)
            )
            match = re.match(rb"^Directory (.*) created$", output)
            if match:
                self.DEST_DIR_ID = match.groups()[0].decode("utf-8")
            else:
                raise Exception(
                    'Cannot create destination directory "{}"'.format(dirname)
                )
        else:
            self.DEST_DIR_ID = (
                re.search(b"\s(.*)\s+" + dirname.encode(), listing, re.MULTILINE)
                .groups()[0]
                .strip()
            ).decode("utf-8")
        return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--sunet",
        action="store",
        dest="sunet",
        help='sunet id (default is local username "{}"'.format(os.environ.get("USER")),
    )
    parser.add_argument(
        "-u",
        "--upload",
        action="store_true",
        help="upload all your data as per detected vars",
    )
    parser.add_argument(
        "-i", "--interactive", action="store_true", help="don't return, go into Ipython"
    )
    args = parser.parse_args()

    migrator = Migrator(sunet=args.sunet)

    logger.info("Detected AFS attributes: {}".format(migrator.src.__dict__))
    logger.info("Detected gDrive attributes: {}".format(migrator.dest.__dict__))

    if args.upload:
        migrator.migrate()

    if args.interactive:
        import IPython  # import IPython; IPython.embed()

        IPython.embed()
